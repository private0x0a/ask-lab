<?php
session_start();
require_once('connect.php');
if (isset($_SESSION['username'])) {
    $username = $_SESSION['username'];
    $update = "UPDATE askApp SET active='0' WHERE username='$username'";
    $result = mysqli_query($connection, $update);
    $_SESSION = array();
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(
            session_name(),
            '',
            time() - 42000,
            $params["path"],
            $params["domain"],
            $params["secure"],
            $params["httponly"]
        );
    }
    session_destroy();
}

header('location: login.php');
 