CREATE DATABASE ask_db;

USE ask_db;

CREATE TABLE askApp (
  id int(11) NOT NULL AUTO_INCREMENT,
  username varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  active tinyint(1) NOT NULL,
  lastLogin datetime DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY username (username)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;