<?php
if (isset($_POST) & !empty($_POST)) {
    foreach ($_POST as $key => $value) {
        if (empty($_POST[$key])) {
            $error_message = "All Fields are required";
            break;
        }
    }

    if ($_POST['password'] != $_POST['passwordConfirmation']) {
        $error_message = 'Passwords should be same<br>';
    }

    if (!isset($error_message)) {
        require_once('connect.php');
        $username = mysqli_real_escape_string($connection, $_POST['username']);
        $password = md5($_POST['password']);
        $passwordConfirmation = md5($_POST['passwordConfirmation']);

        $usernameCheckSql = "SELECT * FROM askApp WHERE username='$username'";
        $usernames = mysqli_query($connection, $usernameCheckSql);
        $count = mysqli_num_rows($usernames);
        if ($count == 1) {
            $error_message = "Username exists in database, use different name";
        } else {
            $createUserSql = "INSERT INTO askApp (username, password, active) VALUES ('$username', '$password', '0')";
            $result = mysqli_query($connection, $createUserSql);
            if ($result) {
                $error_message = "";
                $success_message = "You have registered successfully!";
                unset($_POST);
                unset($username);
            } else {
                $error_message = "Error when saving user to the DB";
                unset($_POST);
            }
        }
    }
}
?>


<html>

<body>

    <h2>Registration form</h2>

    <form name="registrationForm" method="post" action="">
        <?php if (!empty($success_message)) { ?>
        <div class="success-message"><?php if (isset($success_message)) echo $success_message; ?></div>

        <?php 
    } ?>
        <?php if (!empty($error_message)) { ?>
        <div class="error-message"><?php if (isset($error_message)) echo $error_message; ?></div>
        <?php 
    } ?>
        Username:<br>
        <input type="text" name="username" value="<?php if (isset($_POST['username']) & !empty($_POST['username'])) { echo $_POST['username'];} ?>" required>
        <br>
        Password:<br>
        <input type="password" name="password">
        <br>
        Confirm password:<br>
        <input type="password" name="passwordConfirmation">
        <br><br>
        <input type="submit" value="Submit" name='registerUser'>
    </form>
    <a href="login.php">Login</a>
</body>

</html> 